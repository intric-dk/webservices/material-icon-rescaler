# Material Icon Rescaler

Rescale material icons by adding a padding.

The application is live at: https://material-icon-rescaler.intric.dk

## Development

Make sure to have the latest LTS of [NodeJS](https://nodejs.org/en/) installed. Afterwards install the dependencies and run the development server:

```shell
$ npm ci
$ npm start
```

## Deployment

The application can be built by running `npm run build`. The webserver should handle `404 - Not Found` errors by serving the `index.html`.

## License

This project is licensed under the terms of the [MIT license](./LICENSE.md).
