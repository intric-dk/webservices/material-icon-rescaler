import { blue } from '@material-ui/core/colors';
import { createMuiTheme } from '@material-ui/core/styles';

// A custom theme for this app
const theme = createMuiTheme({
  palette: {
    primary: {
      main: blue.A200,
    },
    secondary: {
      main: blue.A100,
    },
  },
});

export default theme;
