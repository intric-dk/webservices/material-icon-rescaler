const endpoint = (color, theme, icon) =>
  `https://material-icons.github.io/material-icons-png/png/${color}/${icon}/${theme}-4x.png`;

const createImage = (url) =>
  new Promise((resolve, reject) => {
    const image = new Image();
    image.addEventListener('load', () => resolve(image));
    image.addEventListener('error', (error) => reject(error));
    image.src = url;
  });

export const normalize = (string) => string.replace(/-/g, '').toLowerCase();

export const loadIcon = async (color, theme, icon) => {
  const normColor = normalize(color);
  const normTheme = normalize(theme);
  const normIcon = normalize(icon);
  try {
    const res = await fetch(endpoint(normColor, normTheme, normIcon));
    if (res.status === 200) {
      const image = await res.blob();
      return URL.createObjectURL(image);
    }
    return null;
  } catch (e) {
    return null;
  }
};

export const deleteImage = (objectUrl) => URL.revokeObjectURL(objectUrl);

export const padImage = async (objectUrl) => {
  // Create image object from URL.
  const imageObj = await createImage(objectUrl);

  // Create canvas and get context.
  const canvas = document.createElement('canvas');
  const ctx = canvas.getContext('2d');

  // Configure canvas size.
  const newSize = 144;
  canvas.width = newSize;
  canvas.height = newSize;

  // Draw image into canvas and save newly created.
  const offset = (newSize - Math.max(imageObj.width, imageObj.height)) / 2;
  ctx.drawImage(imageObj, offset, offset);

  return new Promise((resolve) => {
    canvas.toBlob((file) => {
      resolve(URL.createObjectURL(file));
    }, 'image/png');
  });
};
