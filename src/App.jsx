import React, { useState, useEffect } from 'react';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Select from './Select';
import { loadIcon, deleteImage, padImage, normalize } from './helpers';

const useStyles = makeStyles((theme) => ({
  grid: {
    minHeight: '100vh',
    padding: theme.spacing(1),
  },
  image: {
    height: 144,
    display: 'block',
    margin: '0 auto',
  },
  blackFont: {
    color: '#000',
  },
  blackBackground: {
    backgroundColor: '#fff',
  },
  whiteFont: {
    color: '#fff',
  },
  whiteBackground: {
    backgroundColor: '#000',
  },
  placeholder: {
    height: 144,
  },
}));

const iconFound = 'Icon found!';
const iconNotFound = 'Icon not found!';
const colorOptions = ['Black', 'White'];
const themeOptions = ['Round', 'Baseline', 'Sharp', 'Outline', 'Two-Tone'];

// Create normalized download file name.
const getIconName = (icon, theme, color) =>
  `${normalize(icon)}_${normalize(theme)}_${normalize(color)}.png`;

const App = () => {
  const classes = useStyles();
  const [color, setColor] = useState(colorOptions[0]);
  const [theme, setTheme] = useState(themeOptions[0]);
  const [icon, setIcon] = useState('Explore');
  const [error, setError] = useState(iconFound);
  const [image, setImage] = useState(null);

  // Update the icon.
  const updateIcon = async (color, theme, icon, setFunc, value) => {
    // Update value of input field.
    setFunc(value);

    // Load the image.
    const loadedImage = await loadIcon(color, theme, icon);

    // Check if image could be found.
    if (loadedImage === null) {
      setError(iconNotFound);

      // Make sure to clean up old icons.
      if (image !== null) {
        deleteImage(image);
      }

      setImage(null);
    } else {
      setError(iconFound);

      // Create padded image.
      const paddedImage = await padImage(loadedImage);

      setImage(paddedImage);
    }
  };

  // Define input events.
  const onColor = (e) =>
    updateIcon(e.target.value, theme, icon, setColor, e.target.value);
  const onTheme = (e) =>
    updateIcon(color, e.target.value, icon, setTheme, e.target.value);
  const onIcon = (e) =>
    updateIcon(color, theme, e.target.value, setIcon, e.target.value);

  // Load initial icon.
  useEffect(() => {
    updateIcon(color, theme, icon, () => null, null);

    // Clean up icon before unmount.
    return () => {
      if (image !== null) {
        deleteImage(image);
      }
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // Render markup.
  return (
    <Grid
      className={classes.grid}
      justify="center"
      alignItems="center"
      container
    >
      <Grid xs={12} sm={10} md={8} lg={6} xl={4} item>
        <Card variant="outlined">
          <CardContent>
            <Grid spacing={2} container>
              <Grid xs={12} item>
                <Typography component="h1" variant="h5" align="center">
                  Material Icon Rescaler
                </Typography>
                <Typography component="h2" variant="subtitle1" align="center">
                  Rescale&nbsp;material&nbsp;icons
                  by&nbsp;adding&nbsp;a&nbsp;padding.
                </Typography>
              </Grid>
              <Grid xs={12} item>
                <Select
                  value={color}
                  onChange={onColor}
                  options={colorOptions}
                  label="Color"
                ></Select>
              </Grid>
              <Grid xs={12} item>
                <Select
                  value={theme}
                  onChange={onTheme}
                  options={themeOptions}
                  label="Theme"
                ></Select>
              </Grid>
              <Grid xs={12} item>
                <Typography align="center">
                  A&nbsp;list&nbsp;of&nbsp;all&nbsp;available&nbsp;icon
                  names&nbsp;can&nbsp;be&nbsp;found&nbsp;
                  <Link
                    href="https://material.io/resources/icons"
                    target="_blank"
                    rel="noreferrer"
                  >
                    here
                  </Link>
                  .
                </Typography>
              </Grid>
              <Grid xs={12} item>
                <TextField
                  value={icon}
                  onChange={onIcon}
                  variant="outlined"
                  name="textfield-icon"
                  label="Icon"
                  error={error === iconNotFound}
                  helperText={error}
                  fullWidth
                />
              </Grid>
              <Grid xs={12} item>
                <Card
                  className={classes[`${normalize(color)}Background`]}
                  variant="outlined"
                >
                  <Typography
                    className={classes[`${normalize(color)}Font`]}
                    variant="subtitle1"
                    align="center"
                  >
                    {image !== null ? 'Preview' : 'No preview available!'}
                  </Typography>
                  {image !== null ? (
                    <img className={classes.image} src={image} alt={icon} />
                  ) : (
                    <div className={classes.placeholder}></div>
                  )}
                </Card>
              </Grid>
            </Grid>
          </CardContent>
          <CardActions>
            <Button
              variant="contained"
              color="primary"
              disabled={image === null || error === iconNotFound}
              href={image || '#'}
              download={getIconName(icon, theme, color)}
            >
              Download
            </Button>
          </CardActions>
        </Card>
      </Grid>
    </Grid>
  );
};

export default App;
