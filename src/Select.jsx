import React from 'react';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MuiSelect from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

const Select = ({ options, label, value, onChange }) => (
  <FormControl variant="outlined" fullWidth>
    <InputLabel id={`select-${label}-label`}>{label}</InputLabel>
    <MuiSelect
      labelId={`select-${label}-label`}
      id={`select-${label}`}
      name={label.toLowerCase()}
      value={value}
      onChange={onChange}
      label={label}
    >
      {options.map((option) => (
        <MenuItem key={option} value={option}>
          {option}
        </MenuItem>
      ))}
    </MuiSelect>
  </FormControl>
);

Select.propTypes = {
  options: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default Select;
